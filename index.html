<!DOCTYPE html>
<html>
  <head>
    <title>The Great Gopher Race</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }
      .inverse {
        background: #3f687d;
        color: #aaaba5;
        text-shadow: 0 0 20px #333;
      }
      .inverse h1, .inverse h2 {
        color: #f3f3f3;
        line-height: 0.8em;
      }
      .footnote {
        position: absolute;
        font-size: small;
        bottom: 3em;
        right: 3em;
      }
      blockquote {
        border-left: 0.4em solid #d7d7d7;
        padding-left: 1em !important;
      }
      pre {
        border-left: 0.4em solid #d7d7d7;
        padding: 1em !important;
      }
      code {
        background: #eeeeee !important;
      }
    </style>
  </head>
  <body>
    <textarea id="source">

class: center, middle, inverse

# The Great Gopher Race

@ambot, May 2017

<button value="Toggle" id="btn" onclick="toggle()">▶</button>
<canvas id="myCanvas" height="600" width="600" />


.footnote[
  created with [remark](http://github.com/gnab/remark)
]

---

# Overview

In which I propose:

 * That we introduce the race detector into our build pipelines (`go test -race`).
 * The death-race challenge.

---

# Overview

 0. What's a data race? Why do we care?
 1. The race detector
 2. Examples & fixes
 3. Best practices for avoidance
 4. Mea culpa
 5. Testing
 6. Death Mode

---

# Racewut?

_What's a race condition?_

--

> A race condition or race hazard is the behavior of an electronic, software, or other system where **the output is dependent on the sequence or timing of other uncontrollable events**. 

> It becomes a bug when events do not happen in **the order the programmer intended**. 

_See [Wikipedia](https://en.wikipedia.org/wiki/Race_condition)_

---

# What's a data race?

--

> * A data race occurs when two goroutines access the same variable concurrently and at least one of the accesses is a write.

--

 * A data race is also a race condition when it affects the correctness of your program

--

> * Data races are among the most common and hardest to debug types of bugs in concurrent systems. 

_See [Go documentation](https://golang.org/doc/articles/race_detector.html)_

_See also [this discussion of RC vs DR](http://blog.regehr.org/archives/490)_

---

# Quick example

See [racey1.go](racey1.go)

```
go run racey1.go
```

---

# Other types of race condition?

(What's out of scope for the race detector)

  - Distributed races (timing accross components, app/db/...)
  - 'Channel races' (often cause deadlocks)

_You're on your own with these ones._

--

## Related concurrency problems

 * Deadlocks
 * Livelocks
 * Thread starvation

_These too, mostly._

---

# And then ... ?

Why do we care?

--

> Race conditions have a reputation of being difficult to reproduce and debug

--

 * **Intermittent** - PagerDuty @ 3am, who what how did that happen? 'Can't reproduce it.' 'Works on my machine.' 'Probably k8s or something.'. 'Stupid Gophers'. 'Just restart it'.

--

 * **Elusive** - [Heisenbugs](https://en.wikipedia.org/wiki/Heisenbug) disappear when you observe them. Debugging/logging alters the behaviour. _They commonly behave differently on different hardware (jenkins/docker/...)_

 <image src="http://www.breakingbadgifs.com/gifs/gifs/heisenberg/breaking-bad-gif-heisenberg-984162.gif"></image>

---

# And then ...

 * **Viral** - untreated, they can grow & change as your code changes.

--

 * **Destructive** - [icebugs](http://imadeitup.con) - trigger all kinds of nasties, e.g. deadlocks, inconsistent state ... 

--

 * '**Benign**' data races - [what could go wrong?](https://software.intel.com/en-us/blogs/2013/01/06/benign-data-races-what-could-possibly-go-wrong) ...
 
---

# Something must be done!

<img src="http://pedestriantv-prod.s3.amazonaws.com/images%2Farticle%2F2016%2F12%2F11%2Fawaywithit.gif"></img>

--

 * Tooling
 * Best practice

---

# Go's Data Race Detector

 * Enter stage left:

> `go run/build/install/test -race .`

 * The compiler instruments reads/writes in your code.
 * The runtime watches for unsynchronised access.
 * It's a 'pure happens-before' detector. (There be flavours)
 * It's built on top of ThreadSanitizer (C++, for Chrome)
 * No false-positives; but, can miss races.

 See https://blog.golang.org/race-detector

---

# Example

 * Quick example to play with:

> `go run -race racey1.go`

---

# The ground rules 

golang.org says:

 * Programs that modify data being simultaneously accessed by multiple goroutines must serialize such access.
 * To serialize access, protect the data with channel operations or other synchronization primitives such as those in the sync and sync/atomic packages.
 * See https://golang.org/ref/mem#Advice

--

# 3 examples with fixes:
   
 https://golang.org/doc/articles/race_detector.html#Typical_Data_Races

 * Channels
 * Locks
 * Atomics

---

# Avoiding data races


 * Do not ... instead ...

--

 * [Do not communicate by sharing memory, instead share by communicating](https://blog.golang.org/share-memory-by-communicating)

--
 
 * Prefer channels over mutexes (locks are locky), where appropriate.

---

# Best practice

 * Good practices - <del>KISS</del> MISTY. 
 * Difficulty grows exponentially with complexity. Simplicity wins, yay!
 * Fewer channels, fewer goroutines, fewer mutexes. Just enough synchronization.

--

 * Where possible, a single goroutine should be responsible for accessing a given variable.

--

 * Prefer private variables. Pass-by-value (not pointers) in channels.

---

# Avoiding data races

Sender should not reuse the message after sending:

 * Defensive copy / burn after use. 
 * Suggestion: introduce parameter object 'channelParams'

```go
 c <-entityChannelParams{
    id:       id,
    categories: []string{cat...},
    other:    other,
 }
 
//>```

---

# Avoiding data races

Closures can be problematic because pointers can change between definition and invocation.

  * Beware the special care when iterating in a for loop.
  * Better to pass in a parameter to the closure. `func(j int) {...}(i)`

---

# Avoiding race conditions

## `close(chan)` is your friend

Avoid sending a message when a `close()` will do:

 * Sending messages block.
 * `close()` is like a broadcast. It doesn't block, and can be read by multiple goroutines. 
 * Just watch out - don't call it twice (panic). Instead ...
 * `context.WithCancel()` uses `close()` under the hood, and it's safe to `cancel()` multiple times - yay!

---

# How does this look in the field?

## Mea culpa - data races

--

 * Passing entity across a channel for background processing, then continue to use this entity. _Race to persist between foreground and background tasks (last one wins)._

--

 * Passing stateful context (logger, etc) to workers. By the time the worker uses that state, it's got the wrong context info.

--

 * Racey tests (e.g.: asserting a value updated by another goroutine)

--

 * That thing with for loops and closures (see golang.org examples, above).

---

## Mea culpa - 'channel races':


 * 'Dirty' pipelines cause deadlock.

  - e.g. 'done'/'success' channels not ready to receive when sender sends.
  - e.g. 'main' for/select goroutine blocks in one case.
  
_keep those pipelines 'clean'. Onward receivers should be ready. Upstream senders should be simple._

--

 * Buffered channel hides deadlock. Don't buffer - redesign, simplify.

--

 * Too many synchronizations - many channels, many mutexes, channel+mutex, etc. Try to limit as much as poss. There be little race conditions. MISTY.

--

 * Non-producer waiting on a `sync.WaitGroup()`. Only `Wait()` once producer is done producing. (because it can be zero part-way through processing)

---

# Testing

 * `go test -race` - tests fail when they hit a race condition.
 * Testing is hard. Harder with concurrency.

--

 * A theory: concurrently testable code is inherently _less_ racey.

--

 * `go test -cpu=1,2,4` ... ?

--

# Proposal

 * [Integration tests](https://testing.googleblog.com/2015/04/just-say-no-to-more-end-to-end-tests.html), aka [component tests](https://martinfowler.com/bliki/ComponentTest.html), aka [medium tests](https://testing.googleblog.com/2010/12/test-sizes.html), should run in `-race` mode.

---

# Oh noes racey tests

 * Tests themselves can trigger races (easier to access shared state than to redesign?).
 * We have lots of racey tests...

---

## Testing tips:

 * Use a build flag `// +build !race` to exclude known-to-be-racey test files. Not _every_ test needs to change.

--

 * If you want to assert on a varible it's best to run the 'owning goroutine' in the 'main' goroutine of your test.
 
--

 * Avoid checking state. Assert on a return value where possible. 

--

 * Sometimes, a simple extract-method is best.
 * Otherwise, synchronise on something. context.Context is good.

--

 * Avoid `time.Sleep` in tests, it's a smell. 

--

 * See https://splice.com/blog/lesser-known-features-go-test/
 * See https://blog.codeship.com/creating-fakes-in-go-with-channels/

---

# The death-race

<img src="http://i.imgur.com/BSUQYXD.gif" />

--

 * `go build -race` 
 * `GORACE="halt_on_error=1" ./executable`
 * Dies immediately with exit code 66

---

## The Death-race Challenge

### Expert mode:

 * Run a full test environment in death-race mode

--

### Mega Death mode:

 * Run death-race mode in a prod environment, with a feature flag?!!!
 
 <img src="http://www2.pictures.zimbio.com/mp/IIBbc-2OsrZx.gif">

---

# Discuss ... ?


<img src="http://nerdapproved.com/wp-content/uploads/2015/07/mic-drop.gif">

--

## Further reading

 * [Rust](https://doc.rust-lang.org/nomicon/races.html) is free from data-races, by design
 * A video on the [Go race detector](https://www.infoq.com/presentations/go-race-detector) 
 * [go -cover -race](http://herman.asia/running-the-go-race-detector-with-cover)
 * [Inner workings of the race detector](http://robertknight.github.io/talks/golang-race-detector.html)
 * [ThreadSanitizer](https://github.com/google/sanitizers/wiki/ThreadSanitizerGoManual)
 * https://www.cs.uic.edu/~jalowibd/uploads/06561640_DataRace.pdf
 * [CSP](https://en.wikipedia.org/wiki/Communicating_sequential_processes)
 * [Lock-free programming](http://preshing.com/20120612/an-introduction-to-lock-free-programming/)



  </textarea>
  <script src="https://remarkjs.com/downloads/remark-latest.min.js">
  </script>
  <script>
    var slideshow = remark.create();
  </script>
  <script type="text/javascript">
  var on=false;
  window.addEventListener('load', function () {
    var ctx = document.getElementById('myCanvas').getContext('2d');

      var img = new Image;
      img.src = 'gopher.png';
      img.style.height = '10%';
      img.style.width = '10%';
      img.addEventListener('load', function () {

        var xs = [0,0,0,0,0];
        xoffs = [0,6,1,2,4];
        var interval = setInterval(function() {
          return function () {
            if (on) {
              ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

              for (var i=0;i<5;i++) {
                ctx.drawImage(img, xs[i], i*20);
                xs[i] += 10+xoffs[i]; //(i*Math.random());
                if (xs[i] > ctx.canvas.width) {
                  xs[i] = 0;
                }
              }
            }
          };
        }(), 1000/40);
      }, false);
  }, false);
  var toggle = function() {
    on=!on;
    var btn = document.getElementById('btn');
    if(on) {
      btn.innerHTML = '&#x23f8';
    } else {
      btn.innerHTML = '▶';
    }
  };
  </script>
  </body>
</html>
