The Great Gopher Race
=====================

Some slides and accompanying code for a presentation about race conditions in Go.

View [the slides online](https://bb.githack.com/laher/the-great-gopher-race/raw/master/index.html)

Or open index.html locally in your browser.

![title slide](the-great-gopher-race.gif)

