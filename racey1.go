package main

type counter struct {
	c   int
	max int
}

func (c *counter) increment() {
	if c.c < c.max {
		//runtime.Gosched()
		c.c += 1
	}
}

func (c *counter) count() int {
	return c.c
}

func main() {
	c := &counter{max: 1}
	go c.increment()
	go c.increment()
	c.increment()
	//runtime.Gosched()
	println("Count:", c.count())
}
