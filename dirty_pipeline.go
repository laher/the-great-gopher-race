package main

import "fmt"

func pipeline(ch1, ch2, ch3 chan string, out chan struct{}) {

	for {
		select {
		case s := <-ch1:
			out <- struct{}{}
			fmt.Printf("Received [%s] on ch1\n", s)
		case s := <-ch2:
			fmt.Printf("Received [%s] on ch2\n", s)
		case ch3 <- "hi":
			out <- struct{}{}
		}

	}

}
