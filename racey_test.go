package main

import (
	"math/rand"
	"testing"
	"time"
)

type thing struct{ isOK bool }

func (s *thing) doSomething() {
	// does a thing, expecting to take a short time
	s.isOK = true
}

func (s *thing) doSomeOtherThing() {
	i := 40 / (rand.Intn(2) + 1)
	// does a thing, expecting to take a really short time
	<-time.After(time.Millisecond * time.Duration(i))
}

func TestRaceyTest(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	st := &thing{}
	go func() {
		st.doSomeOtherThing()
		t.Logf("done")
		st.doSomething()
	}()
	func() {
		// do something inline
		//<-time.After(time.Millisecond * 20)
	}()
	for i := 0; i < 30; i++ {
		time.Sleep(time.Millisecond)
		if st.isOK {
			return
		}
	}
	t.Error("something not complete in time")
}
